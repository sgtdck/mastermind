/**
 * Inits the WebGL or returns false if it couldn't be loaded.
 */
export function getWebGL(id: string) {
  const canvas = document.getElementById(id) as HTMLCanvasElement;
  const webgl = canvas.getContext('webgl') || canvas.getContext('experimental-webgl');
  if (!webgl || !(webgl instanceof WebGLRenderingContext)) return false;
  return webgl;
}

/**
 * Creates the vertex buffer, binds it, passes the vortex data to it,
 * and sets the color.
 */
export function initWebGL(webgl: WebGLRenderingContext, vortexes: number[]) {
  const vertexBuffer = webgl.createBuffer();

  webgl.bindBuffer(webgl.ARRAY_BUFFER, vertexBuffer);

  webgl.bufferData(webgl.ARRAY_BUFFER, new Float32Array(vortexes), webgl.STATIC_DRAW);

  webgl.clearColor(0, 0.5, 0.5, 0.9);

  webgl.clear(webgl.COLOR_BUFFER_BIT);
}

/**
 * Creates the vertex shader object from the source code defined in
 * 2_vertex_shader.js.
 */
export function createVertexShader(webgl: WebGLRenderingContext, vertexCode: string) {
  const vertexShader = webgl.createShader(webgl.VERTEX_SHADER);
  webgl.shaderSource(vertexShader, vertexCode);
  webgl.compileShader(vertexShader);

  return vertexShader;
}

/**
 * Creates the fragment shader object from the source code defined in
 * 2_vertex_shader.js.
 */
export function createFragmentShader(webgl: WebGLRenderingContext, fragmentCode: string) {
  const fragmentShader = webgl.createShader(webgl.FRAGMENT_SHADER);

  webgl.shaderSource(fragmentShader, fragmentCode);
  webgl.compileShader(fragmentShader);

  return fragmentShader;
}

/**
 * Create and attach the shader programs from the shader compiled objects.
 */
export function createShaderProgram(webgl: WebGLRenderingContext, vertexShader: WebGLShader, fragmentShader: WebGLShader) {
  const shaderProgram = webgl.createProgram();

  webgl.attachShader(shaderProgram, vertexShader);
  webgl.attachShader(shaderProgram, fragmentShader);
  webgl.linkProgram(shaderProgram);
  webgl.useProgram(shaderProgram);

  return shaderProgram;
}

/**
 * Gets and sets the coordinates associating the compiled shader programs
 * to buffer objects.
 */
export function transformCoordinatesAndSet(webgl: WebGLRenderingContext, shaderProgram: WebGLProgram) {
  const coordinates = webgl.getAttribLocation(shaderProgram, 'coordinates');

  webgl.vertexAttribPointer(coordinates, 2, webgl.FLOAT, false, 0, 0);

  webgl.enableVertexAttribArray(coordinates);
}

/**
 * Draws the arrays.
 */
export function drawArrays(webgl: WebGLRenderingContext) {
  webgl.drawArrays(webgl.TRIANGLES, 0, 3);
}
