import { CodeColors, KeyColors } from '../mastermind';

export const degreesToRadians = (deg: number): number => (Math.PI * deg) / 180;

export const colorToHex = (color: CodeColors | KeyColors): string => {
  switch (color) {
    case CodeColors.Blue:
      return 'blue';
    case CodeColors.Green:
      return 'green';
    case CodeColors.Orange:
      return 'orange';
    case CodeColors.Purple:
      return 'purple';
    case CodeColors.Red:
      return 'red';
    case CodeColors.Yellow:
      return 'yellow';
    case KeyColors.Black:
      return 'black';
    case KeyColors.White:
      return 'white';
    default:
      throw `Unknown color ${color}`;
  }
};
