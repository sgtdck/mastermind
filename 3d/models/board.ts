import * as THREE from 'three';
import { degreesToRadians } from '../helpers';
import { UserData } from './types';

export const generateBoard = (width: number, length: number): THREE.Mesh => {
  const geometry = new THREE.BoxGeometry(width, length, 1);
  geometry.rotateX(-degreesToRadians(90));
  const material = new THREE.MeshBasicMaterial({ color: 'grey' });
  const board = new THREE.Mesh(geometry, material);
  board.userData = { type: 'board' } as UserData;
  return board;
};
