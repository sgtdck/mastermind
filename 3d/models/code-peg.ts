import * as THREE from 'three';
import { CodeColors } from '../../mastermind';
import { colorToHex, degreesToRadians } from '../helpers';
import { UserData } from './types';

export interface CodePegUserData extends UserData {
  color: CodeColors;
}

export const generateCodePeg = (color: CodeColors, segments = 32): THREE.Mesh<THREE.Geometry, THREE.MeshPhongMaterial> => {
  const hexColor = colorToHex(color);

  const geometry = new THREE.Geometry();

  const sphere = new THREE.SphereGeometry(1, segments, segments, 0, Math.PI);
  sphere.rotateX(-degreesToRadians(90));
  const sphereMesh = new THREE.Mesh(sphere);

  const cylinder = new THREE.CylinderGeometry(0.5, 0.5, 1.5, segments);
  const cylinderMesh = new THREE.Mesh(cylinder);

  geometry.merge(sphereMesh.geometry, sphereMesh.matrix);
  geometry.merge(cylinderMesh.geometry, cylinderMesh.matrix);

  const material = new THREE.MeshPhongMaterial({ color: hexColor });
  material.side = THREE.DoubleSide;

  const peg = new THREE.Mesh(geometry, material);
  peg.userData = { type: 'codepeg', color } as CodePegUserData;
  return peg;
};
