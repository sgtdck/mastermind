import * as THREE from 'three';
import { degreesToRadians } from '../helpers';
import { UserData } from './types';
import { CELL_SIZE } from '../../board';

export interface HoleUserData extends UserData {
  x: number;
  y: number;
}

const generateHole = (width: number, length: number, row: number, column: number): THREE.Mesh => {
  const shape = new THREE.Shape();

  const x = column * CELL_SIZE - CELL_SIZE / 2;
  const y = row * CELL_SIZE - CELL_SIZE / 2;

  shape.arc(y, x, 0.5, 0, degreesToRadians(360), false);

  const geometry = new THREE.ExtrudeGeometry(shape, { bevelEnabled: false, depth: 0.1 });
  geometry.rotateX(-degreesToRadians(90));
  geometry.rotateY(degreesToRadians(270));

  const hole = new THREE.Mesh(geometry, new THREE.MeshBasicMaterial({ color: 'white' }));
  hole.position.setX(-(width / 2));
  hole.position.setZ(-(length / 2));
  hole.userData = { type: 'hole', x: row - 1, y: column - 1 } as HoleUserData;

  return hole;
};

export const generateHoles = (width: number, length: number, rows: number, columns: number): THREE.Mesh[] => {
  const holes = [];

  for (let i = 1; i <= rows; ++i) {
    for (let j = 1; j < columns; ++j) {
      holes.push(generateHole(width, length, i, j));
    }
  }

  return holes;
};
