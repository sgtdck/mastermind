import * as THREE from 'three';
import { KeyColors } from '../../mastermind';
import { colorToHex, degreesToRadians } from '../helpers';
import { UserData } from './types';

export interface KeyPegUserData extends UserData {
  color: KeyColors;
}

export const generateKeyPeg = (color: KeyColors, segments = 32): THREE.Mesh<THREE.Geometry, THREE.MeshPhongMaterial> => {
  const hexColor = colorToHex(color);

  const geometry = new THREE.Geometry();

  const sphere = new THREE.SphereGeometry(0.5, segments, segments, 0, Math.PI);
  sphere.rotateX(-degreesToRadians(90));
  const sphereMesh = new THREE.Mesh(sphere);
  sphereMesh.matrix.setPosition(0, 0.25, 0);

  const cylinder = new THREE.CylinderGeometry(0.25, 0.25, 1.25, segments);
  const cylinderMesh = new THREE.Mesh(cylinder);

  geometry.merge(sphereMesh.geometry, sphereMesh.matrix);
  geometry.merge(cylinderMesh.geometry, cylinderMesh.matrix);

  const material = new THREE.MeshPhongMaterial({ color: hexColor });
  material.side = THREE.DoubleSide;

  const peg = new THREE.Mesh(geometry, material);
  peg.userData = { type: 'keypeg', color } as KeyPegUserData;

  return peg;
};
