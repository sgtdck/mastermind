import * as THREE from 'three';
import { CodeColors } from '../../mastermind';
import { degreesToRadians } from '../helpers';
import { generateCodePeg } from './code-peg';

export const generateLegend = (): THREE.Mesh[] => {
  const pegs = [
    generateCodePeg(CodeColors.Blue),
    generateCodePeg(CodeColors.Green),
    generateCodePeg(CodeColors.Orange),
    generateCodePeg(CodeColors.Purple),
    generateCodePeg(CodeColors.Red),
    generateCodePeg(CodeColors.Yellow),
  ];

  pegs.forEach((peg, idx) => {
    peg.position.setZ(16 - idx * 2);
    peg.position.setX(-10);
  });

  return pegs;
};
