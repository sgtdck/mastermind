export type ModelType = 'board' | 'hole' | 'codepeg' | 'keypeg';

export interface UserData {
  type: ModelType;
}
