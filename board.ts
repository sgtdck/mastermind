import * as THREE from 'three';
import { Object3D } from 'three';
import { generateBoard } from './3d/models/board';
import { generateHoles } from './3d/models/holes';
import { generateLegend } from './3d/models/legend';

export enum CellPosition {
  Center = 'Center',
  TopLeft = 'TopLeft',
  TopRight = 'TopRight',
  BottomLeft = 'BottomLeft',
  BottomRight = 'BottomRight',
}

export const CELL_SIZE = 3.2;

export class Board {
  private _width: number;
  private _length: number;
  private _board: THREE.Mesh;
  private _holes: THREE.Mesh[];
  private _legend: THREE.Mesh[];

  constructor(private _scene: THREE.Scene, private _rows = 12, private _columns = 5) {
    this._width = CELL_SIZE * _columns;
    this._length = CELL_SIZE * _rows;
    this._scene.add(this.board);
    this._scene.add(...this.holes);
    this._scene.add(...this.legend);
  }

  placeObject(object: Object3D, x: number, y: number, cellPosition: CellPosition = CellPosition.Center): void {
    const baseX = (y / this._columns) * this._width - this._width / 2;
    const baseY = (x / this._rows) * this._length - this._length / 2;

    let offsetX = 0;
    let offsetY = 0;

    switch (cellPosition) {
      case CellPosition.Center:
        offsetX = CELL_SIZE / 2;
        offsetY = CELL_SIZE / 2;
        break;
      case CellPosition.TopLeft:
        offsetX = CELL_SIZE / 4;
        offsetY = CELL_SIZE / 4;
        break;
      case CellPosition.TopRight:
        offsetX = (CELL_SIZE / 4) * 3;
        offsetY = CELL_SIZE / 4;
        break;
      case CellPosition.BottomLeft:
        offsetX = CELL_SIZE / 4;
        offsetY = (CELL_SIZE / 4) * 3;
        break;
      case CellPosition.BottomRight:
        offsetX = (CELL_SIZE / 4) * 3;
        offsetY = (CELL_SIZE / 4) * 3;
        break;
      default:
        throw `Unknown cell position ${cellPosition}`;
    }

    object.position.setX(baseX + offsetX);
    object.position.setZ(baseY + offsetY);

    this._scene.add(object);
  }

  get rows(): number {
    return this._rows;
  }

  get columns(): number {
    return this._columns;
  }

  get gridHelper(): THREE.GridHelper {
    const size = Math.max(this._width, this._length);
    const divisions = Math.max(this._columns, this._rows);
    const gridHelper = new THREE.GridHelper(size, divisions);
    gridHelper.position.setX(size / 2 - this._width / 2);
    gridHelper.position.setZ(size / 2 - this._length / 2);
    return gridHelper;
  }

  get board(): THREE.Mesh {
    if (this._board) return this._board;

    this._board = generateBoard(this._width, this._length);
    this._board.position.setY(-0.5);

    return this._board;
  }

  get holes(): THREE.Mesh[] {
    if (this._holes) return this._holes;

    this._holes = generateHoles(this._width, this._length, this._rows, this._columns);

    return this._holes;
  }

  get legend(): THREE.Mesh[] {
    if (this._legend) return this._legend;

    this._legend = generateLegend();

    return this._legend;
  }
}
