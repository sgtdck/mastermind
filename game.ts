import { generateCodePeg } from './3d/models/code-peg';
import { generateKeyPeg } from './3d/models/key-peg';
import { Board, CellPosition } from './board';
import { CodeColors, isCorrect, KeyColors, testInput } from './mastermind';

export class Game {
  private completed = false;

  constructor(private board: Board, private code: CodeColors[], private guesses: CodeColors[][] = [[]]) {}

  private get currentRow() {
    return this.guesses.length - 1;
  }

  private get currentColumn() {
    return this.currentGuess.length - 1;
  }

  private get keyColumn() {
    return this.board.columns - 1;
  }

  private get currentGuess(): CodeColors[] {
    return this.guesses[this.guesses.length - 1];
  }

  private displayResults() {
    const [correctPosition, correctColor] = testInput(this.code, this.currentGuess);
    const positions = [CellPosition.TopLeft, CellPosition.TopRight, CellPosition.BottomLeft, CellPosition.BottomRight];

    correctPosition.forEach(() =>
      this.board.placeObject(generateKeyPeg(KeyColors.Black), this.currentRow, this.keyColumn, positions.pop()),
    );
    correctColor.forEach(() => this.board.placeObject(generateKeyPeg(KeyColors.White), this.currentRow, this.keyColumn, positions.pop()));

    if (isCorrect(this.code, this.currentGuess)) {
      alert(`You won! The code was ${JSON.stringify(this.code)}`);
      this.completed = true;
      return;
    }
  }

  private validateState() {
    if (this.completed) return;

    if (this.currentGuess.length === this.code.length) this.displayResults();

    if (this.guesses.length === this.board.rows && this.currentGuess.length === this.code.length) {
      alert(`You lost! The code was ${JSON.stringify(this.code)}`);
      this.completed = true;
    }
  }

  private updateCurrentGuess(color: CodeColors) {
    if (this.currentGuess.length === this.code.length) {
      this.guesses.push([color]);
      return;
    }

    this.currentGuess.push(color);
  }

  placePeg(color: CodeColors): void {
    if (this.completed) return;

    const codePeg = generateCodePeg(color);

    this.updateCurrentGuess(color);
    this.board.placeObject(codePeg, this.currentRow, this.currentColumn);

    this.validateState();
  }
}
