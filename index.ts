import * as THREE from 'three';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';
import { degreesToRadians } from './3d/helpers';
import { CodePegUserData } from './3d/models/code-peg';
import { CodeColors } from './mastermind';
import { Game } from './game';
import { Board } from './board';

const setCamera3d = (scene: THREE.Scene, camera: THREE.PerspectiveCamera) => {
  camera.position.set(0, -8, 32);
  scene.rotateX(degreesToRadians(45));
};

const setCameraTop = (scene: THREE.Scene, camera: THREE.PerspectiveCamera) => {
  camera.position.set(0, 0, 32);
  scene.rotateX(degreesToRadians(90));
};

const setMouseCoordinates = (mouse: THREE.Vector3, clientX: number, clientY: number) => {
  mouse.x = (clientX / window.innerWidth) * 2 - 1;
  mouse.y = -(clientY / window.innerHeight) * 2 + 1;
};

const getRandomColor = (colors: CodeColors[]) => colors[Math.floor(Math.random() * colors.length)];
const generateRandomCode = () => {
  const colors = [CodeColors.Blue, CodeColors.Green, CodeColors.Orange, CodeColors.Purple, CodeColors.Red, CodeColors.Yellow];
  return [getRandomColor(colors), getRandomColor(colors), getRandomColor(colors), getRandomColor(colors)];
};

document.addEventListener('DOMContentLoaded', () => {
  const scene = new THREE.Scene();

  // Creates the board
  const board = new Board(scene);
  // scene.add(game.gridHelper);

  const code = generateRandomCode();
  const game = new Game(board, code);

  const camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);

  // setCameraTop(scene, camera);
  setCamera3d(scene, camera);

  // Creates the renderer and appends it to the <body>
  const renderer = new THREE.WebGLRenderer();
  renderer.setSize(window.innerWidth, window.innerHeight);
  renderer.setClearColor(0xffffff, 1.0);
  document.body.appendChild(renderer.domElement);

  new OrbitControls(camera, renderer.domElement);

  // Adds a light source, without it MeshPhongMaterial will not display
  const light = new THREE.DirectionalLight(0xffffff);
  light.position.set(0, 1, 1).normalize();
  scene.add(light);

  // Initialize raycaster and mouse for click tracking
  const raycaster = new THREE.Raycaster();
  const mouse = new THREE.Vector3();

  function intersectObjects(clientX: number, clientY: number) {
    setMouseCoordinates(mouse, clientX, clientY);
    raycaster.setFromCamera(mouse, camera);

    const intersectedLegend = raycaster.intersectObjects(board.legend, false);

    if (intersectedLegend.length) {
      const [peg] = intersectedLegend;
      const { color } = peg.object.userData as CodePegUserData;
      game.placePeg(color);
    }
  }

  function onClick(event: MouseEvent) {
    const { clientX, clientY } = event;
    intersectObjects(clientX, clientY);
  }

  function onTouchEnd(event: TouchEvent) {
    if (event.targetTouches.length === null) return;
    const { clientX, clientY } = event.targetTouches[0];
    intersectObjects(clientX, clientY);
  }

  const animate = () => {
    requestAnimationFrame(animate);
    // scene.rotation.x -= 0.001;
    // scene.rotation.y -= 0.001;
    // scene.rotation.z -= 0.01;

    renderer.render(scene, camera);
  };

  animate();

  window.addEventListener('click', onClick);
  window.addEventListener('touchstart', onTouchEnd);
});
