import { testInput, CodeColors } from './mastermind';

type Code = [CodeColors, CodeColors, CodeColors, CodeColors];
type Guess = [CodeColors, CodeColors, CodeColors, CodeColors];
type PositionCorect = CodeColors[];
type ColorCorrect = CodeColors[];

const { Red, Blue, Orange, Yellow, Green, Purple } = CodeColors;

const colors = Object.entries({
  '🔴': Red,
  '🔵': Blue,
  '🟠': Orange,
  '🟡': Yellow,
  '🟢': Green,
  '🟣': Purple,
});

const toColorIcon = (color: CodeColors) => colors.find(([, colorId]) => color === colorId)[0];

describe('Mastermind', () => {
  describe('testInput', () => {
    const testCases: [Code, Guess, PositionCorect, ColorCorrect][] = [
      [[Red, Blue, Green, Yellow], [Red, Blue, Green, Yellow], [Red, Blue, Green, Yellow], []],
      [[Red, Blue, Green, Yellow], [Purple, Purple, Blue, Purple], [], [Blue]],
      [[Red, Green, Blue, Purple], [Red, Purple, Purple, Blue], [Red], [Purple, Blue]],
      [
        [Purple, Purple, Purple, Red],
        [Purple, Purple, Red, Purple],
        [Purple, Purple],
        [Red, Purple],
      ],
      [[Purple, Red, Red, Yellow], [Red, Red, Red, Red], [Red, Red], []],
      [[Yellow, Yellow, Blue, Blue], [Yellow, Yellow, Yellow, Blue], [Yellow, Yellow, Blue], []],
    ];

    for (const [code, guess, expectedPositionCorrect, expectedColorCorrect] of testCases) {
      describe(`Code: ${code.map(toColorIcon).join(', ')}`, () => {
        describe(`Guess: ${guess.map(toColorIcon).join(', ')}`, () => {
          it(`should return the correct position pegs (${expectedPositionCorrect.map(toColorIcon).join(', ')})`, () => {
            const [positionCorrect] = testInput(code, guess);

            expect(positionCorrect).toEqual(expectedPositionCorrect);
          });

          it(`should return the correct color pegs (${expectedColorCorrect.map(toColorIcon).join(', ')})`, () => {
            const [, colorCorrect] = testInput(code, guess);

            expect(colorCorrect).toEqual(expectedColorCorrect);
          });
        });
      });
    }
  });
});
