export enum CodeColors {
  Red = 'Red',
  Orange = 'Orange',
  Yellow = 'Yellow',
  Green = 'Green',
  Blue = 'Blue',
  Purple = 'Purple',
}

export enum KeyColors {
  White = 'White',
  Black = 'Black',
}

const findColor = (code: CodeColors[], color: CodeColors) => code.find((v) => v === color);
const hasColorInCorrectPosition = (code: CodeColors[], color: CodeColors, position: number) => code[position] === color;

export const testInput = (code: CodeColors[], input: CodeColors[]): [CodeColors[], CodeColors[]] => {
  // We're mutating these below
  const codeCopy = [...code];
  const inputCopy = [...input];

  const positionCorrect = inputCopy.filter((v, i) => {
    if (!hasColorInCorrectPosition(code, v, i)) return false;

    delete codeCopy[i];
    delete inputCopy[i];
    return true;
  });

  if (positionCorrect.length === code.length) return [positionCorrect, []];

  const colorCorrect = inputCopy.filter((v) => {
    const color = findColor(codeCopy, v);
    if (!color) return false;

    delete codeCopy[codeCopy.indexOf(color)];
    return true;
  });

  return [positionCorrect, colorCorrect];
};

export const isCorrect = (code: CodeColors[], input: CodeColors[]): boolean =>
  input.filter((v, i) => hasColorInCorrectPosition(code, v, i)).length === input.length;
